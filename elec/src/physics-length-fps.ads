package physics.length.fps is

    type FootType is new LengthType ( factor => 0) with null record;
    type YardType is new LengthType ( factor => 0) with null record;
    type RodType is new LengthType ( factor => 0) with null record;
    type ChainType is new LengthType (factor => 0) with null record;
    type LinkType is new LengthType (factor => 0) with null record;
    type FurlongType is new LengthType (factor => 0) with null record;
    type MileType is new LengthType (factor => 0) with null record;

    function Value (y : YardType'Class) return FootType;
    function Value (R : RodType'Class) return YardType;
    function Value (C : ChainType'Class) return LinkType;
    function Value (F : FurlongType'Class) return LinkType;
    function Value (F : FurlongType'Class) return YardType;
    function Value (M : MileType'Class) return FurlongType;

end physics.length.fps;
package body physics.electricity is

    function Current(V : VoltageType'Class ; R : ResistanceType'Class) return CurrentType is
        result : CurrentType(v.factor - r.factor);
    begin
        result.value := V.value / R.value;
        return result;
    end Current;

    function Power(V : VoltageType'Class; R : ResistanceType'Class) return PowerType is
        result : PowerType ( v.factor*2 - r.factor);
    begin
        result.value := v.value**2 / R.value;
        return result;
    end Power;

    function Power(I : CurrentType'Class; R : ResistanceType'Class) return PowerType is
        result : PowerType(I.factor*2 + R.factor);
    begin
        result.value := I.value**2 * R.value;
        return result;
    end Power;

    function Energy( p : PowerType'Class; d : Duration) return EnergyType is
        result : EnergyType ( P.factor );
    begin
        result.Value := P.value / Float(d) ;
        return result;
    end Energy;


    function Value(r : ResistanceType'Class) return Float is
    begin
        return r.Value * 10.0 ** r.factor;
    end Value;

    function Value(v : VoltageType'Class) return Float is
    begin
        return v.Value * 10.0 ** v.factor;
    end Value;

    function Value(c : CurrentType'Class) return Float is
    begin
        return c.Value * 10.0 ** c.factor;
    end Value;

    function Value(p : PowerType'Class) return Float is
    begin
        return p.Value * 10.0 ** p.factor;
    end Value;

    function Value(e : EnergyType'Class) return Float is
    begin
        return e.Value * 10.0 ** e.factor;
    end Value;

    function Series(r1, r2 : ResistanceType'Class) return OhmsType is
        result : OhmsType;
    begin
        result.value := Value(r1) + Value(r2);
        return result;
    end Series;

    function Parallel(r1, r2 : ResistanceType'Class) return OhmsType is
        result : OhmsType;
        r1v : constant Float := Value(r1) ;
        r2v : constant Float := Value(r2) ;
    begin
        result.value := r1v*r2v/(r1v + r2v);
        return result;
    end Parallel;

    function Value(r : ResistanceType'Class) return OhmsType is
        result : OhmsType ;
    begin
        result.Value := Value(r);
        return result;
    end Value;

    function Value(r : ResistanceType'Class) return KiloOhmsType is
        result : KiloOhmsType ;
    begin
        result.Value := Value(r) / (10.0 ** 3) ;
        return result;
    end Value;

    function Value(r : ResistanceType'Class) return MegaOhmsType is
        result : MegaOhmsType;
    begin
        result.Value := Value(r) / (10.0 ** 6);
        return result;
    end Value;

    function Value(c : CurrentType'Class) return AmperesType is
        result : AmperesType;
    begin
        result.Value := Value(c) ;
        return result;
    end Value;

    function Value(c : CurrentType'Class) return MilliAmperesType is
        result : MilliAmperesType;
    begin
        result.Value := Value(c) * 10.0 ** 3 ;
        return result;
    end Value;

    function Value(c : CurrentType'Class) return MicroAmperesType is
        result : MicroAmperesType;
    begin
        result.Value := Value(c) * 10.0 ** 6 ;
        return result;
    end Value;

    function Value(v : VoltageType'Class) return VoltsType is
        result : VoltsType;
    begin
        result.Value := Value(v);
        return result;
    end Value;

    function Value(v : VoltageType'Class) return MilliVoltsType is
        result : MilliVoltsType;
    begin
        result.Value := Value(v) * 10.0 ** 3;
        return result;
    end Value;

    function Value(v : VoltageType'Class) return MicroVoltsType is
        result : MicroVoltsType;
    begin
        result.Value := Value(v) * 10.0 ** 6;
        return result;
    end Value;

end physics.electricity;
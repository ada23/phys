package physics is
    function Prefix(power : integer) return String;
    type QuantityType (factor : Integer) is abstract tagged
    record
        value : Float := 0.0;
    end record;
    function Image( q : QuantityType'Class) return String;
end physics;

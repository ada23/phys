package physics.thermal is

    type UnitsType is
    (
        kelvin,
        fahrenheit,
        celsius
    );

    type TemperatureType (unit : UnitsType) is private;
    procedure Set(t : out TemperatureType; v : Float; u : UnitsType );
    procedure Set(tout : out TemperatureType; tin : TemperatureType);
    function Convert(t : TemperatureType; uout : UnitsType) return TemperatureType;

    function Get(t : TemperatureType; u : UnitsType) return Float ;

    abszero : constant TemperatureType;
    Water_Freezpoint : constant TemperatureType;
    Water_Boilingpoint : constant TemperatureType;

    f_to_c : constant Float := 5.0/9.0;
    c_to_f : constant Float := 9.0/5.0;
    c_k : constant Float := 273.15;
    cz_f : constant Float := 32.0;
private
    type TemperatureType (unit : UnitsType) is 
            new QuantityType (factor => 1) with null record;

    abszero : constant TemperatureType := ( unit => kelvin , value => 0.0 );
    Water_Freezpoint : constant TemperatureType := ( unit => celsius , value => 0.0 );
    Water_Boilingpoint : constant TemperatureType := ( unit => fahrenheit , value => 212.0 );

end physics.thermal;
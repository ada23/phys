package body physics.length.fps is

   function Value(y : YardType'Class) return FootType is
        result : FootType;
    begin
        result.value := 3.0 * Y.Value;
        return result;
    end Value;

    function Value (R : RodType'Class) return YardType is
        result : YardType;
    begin
        result.Value := 5.5 * R.Value;
        return result;
    end Value;

    function Value(C : ChainType'Class) return RodType is
        result : RodType ;
    begin
        result.Value := 4.0 * C.Value;
        return result;
    end Value;

    function Value(C : ChainType'Class) return LinkType is
       result : LinkType ;
    begin
        result.Value := 100.0 * C.Value ;
        return result;
    end Value;

    -- Furlong = 10 chains Chain = 100 Links
    function Value(F : FurlongType'Class) return LinkType is
        result : LinkType;
    begin
        result.Value := 10.0 * 100.0 * F.Value;
        return result;
    end Value;

    -- Furlong = 660 feet = 220 yards
    function Value (F : FurlongType'Class) return YardType is
        result : YardType ;
    begin
        result.Value := 220.0 * F.Value;
        return result;
    end Value;

    function Value(M : MileType'Class) return FurlongType is
        result : FurlongType;
    begin
        result.Value := 8.0 * m.Value;
        return result;
    end Value;

end physics.length.fps;
package body physics.length.mks is
    function Value(k : KiloMeterType'Class) return MeterType is
        result : MeterType;
    begin
        result.Value := 1000.0 * k.value;
        return result;
    end Value;

    function Value(m : MeterType'Class) return MilliMeterType is
        result : MilliMeterType ;
    begin
        result.Value := 1000.0 * m.Value;
        return result;
    end Value;

    function Value(m : MeterType'Class) return CentiMeterType is
        result : CentiMeterType;
    begin
        result.Value := 100.0 * m.Value;
        return result;
    end Value;

    function Value(m : MeterType'Class) return NanoMeterType is
        result : NanoMeterType ;
    begin
        result.Value := 1_000_000_000.0 * m.Value;
        return result;
    end Value;

end physics.length.mks;
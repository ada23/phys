package physics.electricity is

    type ResistanceType is new QuantityType with null record;
    type VoltageType is new QuantityType with null record; 
    type CurrentType is new QuantityType with null record;
    type PowerType is new QuantityType with null record; 
    type EnergyType is new QuantityType with null record;

    function Current(V : VoltageType'Class; R : ResistanceType'Class) return CurrentType;
    function Power(V : VoltageType'Class; R : ResistanceType'Class) return PowerType;
    function Power(I : CurrentType'Class; R : ResistanceType'Class) return PowerType;
    function Energy(P : PowerType'Class; d : Duration) return EnergyType;

    function Value(r : ResistanceType'Class) return Float;
    function Value(v : VoltageType'Class) return Float;
    function Value(c : CurrentType'Class) return Float;
    function Value(p : PowerType'Class) return Float;
    function Value(e : EnergyType'Class) return Float;
            
    type OhmsType is new ResistanceType (factor => 0) with null record;
    type KiloOhmsType is new ResistanceType (factor => 3) with null record;
    type MegaOhmsType is new ResistanceType (factor => 6) with null record;

    type VoltsType is new VoltageType (factor => 0) with null record;
    type MilliVoltsType is new VoltageType (factor => -3) with null record;
    type MicroVoltsType is new VoltageType (factor => -6) with null record;

    type AmperesType is new CurrentType (factor => 0) with null record;
    type MilliAmperesType is new CurrentType (factor => -3) with null record;
    type MicroAmperesType is new CurrentType (factor => -6) with null record;

    type WattsType is new PowerType( factor => 0) with null record;
    type MilliWattsType is new PowerType( factor => -3 ) with null record;
    type MicroWattsType is new PowerType( factor => -6) with null record;
    type KiloWattsType is new PowerType( factor => 3) with null record;
    type MegaWattsType is new PowerType( factor => 6) with null record;

    function Value(r : ResistanceType'Class) return OhmsType;
    function Value(r : ResistanceType'Class) return KiloOhmsType;
    function Value(r : ResistanceType'Class) return MegaOhmsType;

    function Value(v : VoltageType'Class) return VoltsType;
    function Value(v : VoltageType'Class) return MilliVoltsType;
    function Value(v : VoltageType'Class) return MicroVoltsType;

    function Value(c : CurrentType'Class) return AmperesType;
    function Value(c : CurrentType'Class) return MilliAmperesType;
    function Value(c : CurrentType'Class) return MicroAmperesType;

    function Series(r1, r2 : ResistanceType'Class) return OhmsType;
    function Parallel(r1, r2 : ResistanceType'Class) return OhmsType;

end physics.electricity;

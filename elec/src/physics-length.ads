package physics.length Is 

    type LengthType is new QuantityType with null record;
    type AreaType is new QuantityType with null record;
    type VolumeType is new QuantityType with null record;
    procedure Init;
end physics.length;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Float_Text_Io; use Ada.Float_Text_Io;
with physics.electricity; use physics.electricity;
with physics.thermal; use physics.thermal;
with physics.length; use physics.length;
with physics.length.mks; use physics.length.mks;
with physics.length.fps; use physics.length.fps;

procedure Elec is
   use physics;
   v1 : MilliVoltsType ;
   r1 : KiloOhmsType;
   procedure resistance_combinations is
      r1 : KiloOhmsType;
      r2 : KiloOhmsType;
      rs : KiloOhmsType;
      rp : KiloOhmsType;
   begin
      r1.Value := 1000.0;
      r2.Value := 10000.0;
      rs := Value(Series(r1,r2));
      Put("Series "); Put_Line(Image(rs));
      rp := Value(Parallel(r1,r2));
      Put("Parallel "); Put_Line(Image(rp));
   end resistance_combinations;

   procedure Therm is
      procedure allunits(title : String; t : TemperatureType) is
      begin
         Put("Title "); Put(title);
         Put(" celsius: "); Put(Get(t,celsius));
         Put(" fahrenheit: " ); Put(Get(t,fahrenheit));
         Put(" kelvin: "); Put(Get(t,kelvin));
         New_Line;
      end allunits;
   begin
      Put_Line("Thermal Tests=============");
      allunits("Abs zero",abszero);
      allunits("Water Freezepoint",Water_Freezpoint);
      allunits("Water Boilingpoint",Water_Boilingpoint);
   end Therm;
   procedure Length is
     height : physics.length.mks.MeterType;
   begin
     height.Value := 1.5;
      Put("Height "); Put( physics.Image(height) ); Put_Line("Meter");
   end Length;
   procedure basic_elec is
   begin
      v1.Value := 2.0;
   r1.Value := 10.0;
   Put_Line("---------------Voltage------------");
   declare
      v : VoltsType := Value(v1);
      vmicro : MicroVoltsType := Value(v1);
      vmilli : MilliVoltsType := value(v1);
   begin
      Put_Line(Image(v));
      Put_Line(Image(vmilli));
      Put_Line(Image(vmicro));
   end;
   Put_Line("---------------Current Calc---------------");
   declare
      c : constant CurrentType := Current(v1,r1);
      c1 : constant AmperesType := Value(c);
      mc1 : constant MilliAmperesType := Value(c);
      mmc1 : constant MicroAmperesType := Value(c);
   begin
      Put_Line(Image(c));
      Put_Line(Image(c1));
      Put_Line(Image(mc1));
      Put_Line(Image(mmc1));
   end;
   Put_Line("------------Power-----------");
   declare
      p : PowerType := Power( v1 , r1 );
   begin
      Put_Line(Image(p));
   end;
   end basic_elec;
   procedure volt_current is
      v1 : VoltsType;
      v2 : MilliVoltsType;
      v3 : MicroVoltsType;
   begin
      v1.Value := 1.0;
      v2.Value := 1.0;
      v3.Value := 1.0;
      Put_Line(Image(v1));
      Put_Line(Image(v2));
      Put_Line(Image(v3));
   end volt_current;

begin
   volt_current;
   resistance_combinations;
   therm;
   Length;
end Elec;

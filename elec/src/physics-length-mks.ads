package physics.length.mks is

    type MeterType is new LengthType ( factor => 0 ) with null record;
    type CentiMeterType is new LengthType( factor => -2 ) with null record;
    type MilliMeterType is new LengthType( factor => -3 ) with null record;
    type NanoMeterType is new LengthType( factor => -9 ) with null record;
    type KiloMeterType is new LengthType( factor => 3 ) with null record;

    function Value(k : KiloMeterType'Class) return MeterType;
    function Value(m : MeterType'Class) return MilliMeterType;
    function Value(m : MeterType'Class) return CentiMeterType;
    function Value(m : MeterType'Class) return NanoMeterType;

    type MeterSqType is new LengthType ( factor => 0 ) with null record;

    type MeterCubeType is new LengthType( factor => 0 ) with null record;
    type LiterType is new LengthType( factor => -3 ) with null record;
    type MilliLiterType is new LengthType ( factor => -6 ) with null record;
    type NanoLiterType is new LengthType ( factor => -9 ) with null record;

end physics.length.mks ;

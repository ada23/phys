package body physics is
    function Prefix(power : Integer) return String is
    begin
        case power is
            when 1 => return "Deca" ;
            when 2 => return "Hecto" ;
            when 3 => return "Kilo";
            when 6 => return "Mega";
            when 9 => return "Giga";
            when 12 => return "Tera";
            when 15 => return "Peta";
            when 18 => return "Exa";
            when 21 => return "Zetta";
            when 24 => return "Yotta";
            when 27 => return "Ronna";
            when 30 => return "Quetta";

            when -1 => return "Deci";
            when -2 => return "Centi";
            when -3 => return "Milli";
            when -6 => return "Micro";
            when -9 => return "Nano";
            when -12 => return "Pico";
            when -15 => return "Femto";
            when -18 => return "Atto";
            when -21 => return "Zepto";
            when -24 => return "Yocto";
            when -27 => return "Ronto";
            when -30 => return "Quecto";
            when others => return "";
        end case;
    end Prefix; 

    function Image( q : QuantityType'Class) return String is
       pfx : String := Prefix(q.factor);
    begin
        if q.factor = 0
        then
            return Float'Image(q.value) & " ";
        end if;
        if pfx'Length > 1
        then
            return Float'Image(q.value) & " " & Prefix(q.factor) & " ";
        end if;
        return Float'Image( q.Value ) & "* 10^" & Integer'Image(q.factor) & " " ;
    end Image;
end physics;
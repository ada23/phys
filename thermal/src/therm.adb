with Ada.Float_Text_Io; use Ada.Float_Text_Io;
with Ada.Text_Io; use Ada.Text_Io;
with Temperature;
procedure Therm is
   procedure allunits(title : String; t : temperature.TemperatureType) is
   begin
      Put("Title "); Put(title);
      Put(" celsius: "); Put(temperature.Get(t,temperature.celsius));
      Put(" fahrenheit: " ); Put(temperature.Get(t,temperature.fahrenheit));
      Put(" kelvin: "); Put(temperature.Get(t,temperature.kelvin));
      New_Line;
   end allunits;
begin
   allunits("Abs zero",temperature.abszero);
   allunits("Water Freezepoint",temperature.Water_Freezpoint);
   allunits("Water Boilingpoint",temperature.Water_Boilingpoint);
end Therm;

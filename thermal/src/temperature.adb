package body temperature is

    procedure Set(t : out TemperatureType; v : Float; u : UnitsType ) is
    begin
        t := ( unit => u, value => v);
    end Set;

    procedure Set(tout : out TemperatureType; tin : TemperatureType) is
    begin
        tout := Convert(tin, tout.unit);
    end Set;

    function Convert(t : TemperatureType; uout : UnitsType) return TemperatureType is
        result : TemperatureType := (unit => uout, value => 0.0 );
    begin
        result.Value := Get(t,uout);
        return result;
    end Convert;
    function Convert(v : float; from: UnitsType; to : UnitsType) return Float is
    begin
        if from = to  
        then
            return v ;
        end if;
        case from is
            when kelvin =>
                case to is
                    when kelvin => null ; 
                    when celsius =>  return v - c_k;
                    when fahrenheit => return cz_f + (v - c_k) * c_to_f;
                end case;            
            when celsius =>
                case to is
                    when kelvin => return c_k + v;
                    when celsius =>  null;
                    when fahrenheit => return cz_f + v * c_to_f;
                end case;            
            when fahrenheit =>
                case to is
                    when kelvin => return c_k + (v - cz_f) * f_to_c ; 
                    when celsius =>  return (v - cz_f) * f_to_c ;
                    when fahrenheit => null;
                end case;
        end case;
    end Convert;

    function Get(t : TemperatureType; u : UnitsType) return Float is
    begin
        if t.unit = u
        then
            return t.Value;
        end if;
        return Convert(t.value,t.unit,u);
    end Get;

end temperature;